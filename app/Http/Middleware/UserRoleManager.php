<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class UserRoleManager
{
    private const MANAGER = 'manager';
    private Auth $auth;

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next): JsonResponse
    {
        $userRole = $this->auth::user()?->role;
        if ($userRole !== self::MANAGER) {
            return response()->json(['messages' => 'no rights'])->setStatusCode(Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
