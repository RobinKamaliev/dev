<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Filters\FormFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Form\StoreRequest;
use App\Http\Requests\Api\Form\UpdateRequest;
use App\Http\Resources\Form\FormResource;
use App\Models\Form;
use App\Services\Api\Models\FormService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Collection;

class FormController extends Controller
{
    public function index(FormService $service): AnonymousResourceCollection
    {
        $formsAuthUser = $service->index();
        return FormResource::collection($formsAuthUser);
    }

    public function store(FormService $service, StoreRequest $request): FormResource
    {
        $form = $service->store($request->validated());
        return FormResource::make($form);
    }

    public function show(Form $form)
    {
        return FormResource::make($form);
    }

    public function update(FormService $service, Form $form, UpdateRequest $request): FormResource
    {
        $form = $service->show($form, $request->validated());
        return FormResource::make($form);
    }

    public function allForms(FormFilter $filter): Collection
    {
        return Form::filter($filter)->get();
    }
}
