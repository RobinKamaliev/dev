<?php

namespace App\Http\Requests\Api\Form;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'comment' => ['required','min:5','max:250'],
        ];
    }
}
