<?php

declare(strict_types=1);

namespace App\Services\Api\Models;

use App\Models\Form;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class FormService
{
    public function index(): Collection
    {
        return Auth::user()->forms;
    }

    public function store(array $param): Form
    {
        $form = new Form();
        $form->fill($param);
        $form->user_id = auth()->user()->id;
        $form->saveOrFail();

        return $form;
    }

    public function show(Form $form, array $param): Form
    {
        if ($form->status == 'in progress') {
            $form->status = 'decided';
        } else {
            $form->status = 'in progress';
        }
        $form->fill($param);
        $form->save();

        return $form;
    }
}
