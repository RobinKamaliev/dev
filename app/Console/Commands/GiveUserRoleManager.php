<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class GiveUserRoleManager extends Command
{
    protected $signature = 'app:user-role-manager';

    protected $description = 'Give user role manager';
    private const MANAGER = 'manager';

    public function handle(): void
    {
        $userId = $this->ask('Enter user id?');
        $user = User::find($userId);
        if ($user !== null) {
            $user->role = self::MANAGER;
            $user->save();
            $this->info('Saved');
        } else  $this->info('User id "' . $userId . '" will not find!');
    }
}
