<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

class FormFilter extends QueryFilter
{
    protected function where(array $allQueryParam): Builder
    {
        foreach ($allQueryParam as $queryParam) {
            $comparisonOperator = $this->getComparisonOperator($queryParam);
            $this->builder->where(current($queryParam), $comparisonOperator, end($queryParam));
        }

        return $this->builder;
    }

    protected function orWhere(array $allQueryParam): Builder
    {
        foreach ($allQueryParam as $queryParam) {
            $comparisonOperator = $this->getComparisonOperator($queryParam);
            $this->builder->orWhere(current($queryParam), $comparisonOperator, end($queryParam));
        }

        return $this->builder;
    }

    protected function with(array|string $queryParam): Builder
    {
        return $this->builder->when($queryParam, function ($query) use ($queryParam) {
            $query->with($queryParam);
        });
    }

    protected function paginate(int $count): Builder
    {
        return $this->builder->when($count, function ($query) use ($count) {
            $query->paginate($count);
        });
    }
}
